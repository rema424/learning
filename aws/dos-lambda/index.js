"use strict";

const https = require("https");

const makeRequests = (event, iteration, callback) => {
  const req = https.request(event.options, res => {
    let body;
    console.log("Status:", res.statusCode);
    res.setEncoding("utg8");
    res.on("data", chunk => (body += chunk));
    res.on("end", () => {
      console.log(
        "Successfully processed HTTPS response, iteration: ",
        iteration
      );

      if (res.headers["content-type"] === "application/json") {
        console.log(JSON.parse(body));
      }
    });
  });
  return req;
};

exports.handler = (event, context, callback) => {
  for (let i = 0; i < 200; i++) {
    const req = makeRequests(event, i, callback);
    req.end();
  }
};
