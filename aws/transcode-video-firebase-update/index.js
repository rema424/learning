"use strict";

const admin = require("firebase-admin");
const serviceAccount = require(process.env.SERVICE_ACCOUNT);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.DATABASE_URL
});

exports.handler = (event, context, callback) => {
  const message = JSON.parse(event.Records[0].Sns.Message);
  const key = message.Records[0].s3.object.key;
  const bucket = message.Records[0].s3.bucket.name;
  const sourceKey = decodeURIComponent(key.replace(/\+/g, " "));
  const uniqueVideoKey = sourceKey.split("/")[0];

  const database = admin.database().ref();

  // トランスコードが終了したことをfirebaseに通知する
  database
    .child("videos")
    .child(uniqueVideoKey)
    .set({ transcoding: false, key, bucket })
    .catch(err => {
      callback(err);
    });
};
