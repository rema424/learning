"use strict";

const AWS = require("aws-sdk");
const crypto = require("crypto");

const s3 = new AWS.S3();

const createErrorResponse = (code, message) => {
  const response = {
    statusCode: code,
    headers: { "Access-Control-Allow-Origin": "*" },
    body: JSON.stringify({ message })
  };
  return response;
};

const createSuccessResponse = message => {
  const response = {
    statusCode: 200,
    headers: { "Access-Control-Allow-Origin": "*" },
    body: JSON.stringify(message)
  };
  return response;
};

/**
 * 与えられたバッファー（文字列化されたドキュメント）をbase64形式に変換する
 * @param {*} value
 */
const base64encode = value => {
  return new Buffer(value).toString("base64");
};

/**
 * 1日後の日付を生成する
 */
const generateExpirationDate = () => {
  let currentDate = new Date();
  currentDate = currentDate.setDate(currentDate.getDate() + 1);
  return new Date(currentDate).toISOString();
};

/**
 * ファイル名の衝突を避けるためのプレフィックスを作成する
 * @param {*} filename
 */
const generatePrefix = filename => {
  console.log("jeneratePrefix start");
  const directory = crypto.randomBytes(20).toString("hex");
  console.log("directory: ", directory);
  const key = directory + "/" + filename;
  return key;
};

/**
 * ポリシードキュメントを作成する
 * @param {*} filename
 * @param {*} key
 */
const generatePolicyDocument = key => {
  const expiration = generateExpirationDate();
  const policy = {
    expiration,
    conditions: [
      { key },
      { bucket: process.env.UPLOAD_BUCKET },
      { acl: "private" },
      ["starts-with", "$Content-Type", ""]
    ]
  };
  return policy;
};

/**
 * ポリシーをbase64形式に変換する
 * @param {*} policy
 */
const encode = policy => base64encode(JSON.stringify(policy)).replace("\n", "");

/**
 * IAMユーザーのシークレットアクセスキーを使ってポリシーからHMAC署名を作成する
 * @param {*} encoding
 */
const sign = encoding =>
  crypto
    .createHmac("sha1", process.env.SECRET_ACCESS_KEY)
    .update(encoding)
    .digest("base64");

/**
 * アップロードポリシーを返却する
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
exports.handler = (event, context, callback) => {
  let filename = null;
  console.log(event.queryStringParameters);

  if (event.queryStringParameters && event.queryStringParameters.filename) {
    console.log(event.queryStringParameters);
    console.log(event.queryStringParameters.filename);
    filename = decodeURIComponent(event.queryStringParameters.filename);
    console.log("filename: ", filename);
  } else {
    callback(null, createErrorResponse(500, "Filename must be provided"));
    return;
  }

  try {
    const key = generatePrefix(filename);
    console.log("key: ", key);
    const policy = generatePolicyDocument(key);
    console.log("policy: ", policy);
    const encoding = encode(policy);
    console.log("encoding: ", encoding);
    const signature = sign(encoding);
    console.log("signature: ", signature);

    const uploadUrl = process.env.UPLOAD_URI + "/" + process.env.UPLOAD_BUCKET;
    console.log("uploadUrl: ", uploadUrl);

    const result = {
      signature,
      encoded_policy: encoding,
      access_key: process.env.ACCESS_KEY,
      upload_url: uploadUrl,
      key
    };

    callback(null, createSuccessResponse(result));
  } catch (err) {
    console.log("error");
    callback(null, createErrorResponse(500, err));
  }
};
