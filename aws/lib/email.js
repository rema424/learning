"use strict";

const AWS = require("aws-sdk");
const SES = new AWS.SES();

const createMessage = (toList, fromEmail, subject, message) => {
  const params = {
    Source: fromEmail,
    Destination: { ToAddress: toList },
    Message: {
      Subject: {
        Data: subject
      },
      Body: {
        Text: {
          Data: message
        }
      }
    }
  };
  return params;
};

const dispatch = async params => {
  try {
    const data = await SES.sendEmail(params).promise();
    return data;
  } catch (err) {
    throw new Error(err);
  }
};

const send = async (toList, fromEmail, subject, message) => {
  try {
    const params = createMessage(toList, fromEmail, subject, message);
    const result = await dispatch(params);
    callback(null, result);
  } catch (err) {
    callback(err);
  }
};

module.exports = { send };
