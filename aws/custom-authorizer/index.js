"use strict";
var jwt = require("jsonwebtoken");
var axios = require("axios");

/**
 * ポリシーを作成する
 * @param {*} principalId
 * @param {*} effect
 * @param {*} resource
 */
var generatePolicy = function(principalId, effect, resource) {
  var authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    var policyDocument = {};
    policyDocument.Version = "2012-10-17";
    policyDocument.Statement = [];

    var statementOne = {};
    statementOne.Action = "execute-api:Invoke";
    statementOne.Effect = effect;
    statementOne.Resource = resource;

    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
};

/**
 * トークンに紐づく証明書を取得する
 * @param {*} token
 */
var fetchAuth0Certfication = async function(token) {
  // jwtをデコードする
  const decodedToken = jwt.decode(token, { complete: true });

  if (!decodedToken) {
    throw new Error("Failed jwt verification");
  }

  // ヘッダー情報のkidプロパティをkidという変数に格納する
  const { kid } = decodedToken.header;

  try {
    var url = `https://${process.env.DOMAIN}/.well-known/jwks.json`;

    // レスポンスのdataプロパティをjwksという変数に格納する
    const { data: jwks } = await axios.get(url);

    // デコードされたjwtのkidとjwksのkidが一致するものを取得する
    const jwksKey = jwks.keys.find(key => key.kid === kid);

    // 証明書を取得する
    let cert = jwksKey.x5c[0];

    // 64文字ごとに改行する
    cert = cert.match(/.{1,64}/g).join("\n");
    cert = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----\n`;

    return cert;
  } catch (e) {
    throw new Error(e);
  }
};

/**
 * JWTの有効性を判定する
 * @param {*} token
 * @param {*} cert
 */
var jwtVerify = async function(token, cert) {
  jwt.verify(token, cert, function(err, decode) {
    if (err) {
      throw new Error(err);
    }
    return decode;
  });
};

/**
 * JWTの正当性をチェックする
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
exports.handler = async function(event, context, callback) {
  if (!event.authorizationToken) {
    callback("Could not find authToken");
    return;
  }

  var authToken = event.authorizationToken;
  var tokens = authToken.split(" ");
  var idToken = tokens[1];
  var accessToken = tokens[2];
  // var secretBuffer = new Buffer(process.env.AUTH0_SECRET);

  try {
    var cert = await fetchAuth0Certfication(idToken);
    await jwtVerify(idToken, cert);
    callback(null, generatePolicy("user", "allow", event.methodArn));
  } catch (e) {
    console.log("Failed jwt verification: ", e, "auth: ", authToken);
    callback("Authorization Failed");
  }
};
