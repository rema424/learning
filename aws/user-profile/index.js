"use strict";
var jwt = require("jsonwebtoken");
var axios = require("axios");

/**
 * トークンに紐づく証明書を取得する
 *
 * @param {*} token
 */
async function fetchAuth0Certfication(token) {
  // jwtをデコードする
  const decodedToken = jwt.decode(token, { complete: true });

  if (!decodedToken) {
    throw new Error("Failed jwt verification");
  }

  // ヘッダー情報のkidプロパティをkidという変数に格納する
  const { kid } = decodedToken.header;

  try {
    var url = `https://${process.env.DOMAIN}/.well-known/jwks.json`;

    // レスポンスのdataプロパティをjwksという変数に格納する
    const { data: jwks } = await axios.get(url);

    // デコードされたjwtのkidとjwksのkidが一致するものを取得する
    const jwksKey = jwks.keys.find(key => key.kid === kid);

    // 証明書を取得する
    let cert = jwksKey.x5c[0];

    // 64文字ごとに改行する
    cert = cert.match(/.{1,64}/g).join("\n");
    cert = `-----BEGIN CERTIFICATE-----\n${cert}\n-----END CERTIFICATE-----\n`;

    return cert;
  } catch (e) {
    throw new Error(e);
  }
}

/**
 * JWTの有効性を判定する
 *
 * @param {*} token
 * @param {*} cert
 */
async function jwtVerify(token, cert) {
  jwt.verify(token, cert, function(err, decode) {
    if (err) {
      throw new Error(err);
    }
    return decode;
  });
}

/**
 * ユーザープロフィール情報を取得する
 *
 * @param {*}} event
 * @param {*} context
 * @param {*} callback
 */
exports.handler = async function(event, context, callback) {
  if (!event.authToken) {
    callback("Could not find authToken");
    return;
  }

  var authToken = event.authToken;
  var tokens = authToken.split(" ");
  var idToken = tokens[1];
  var accessToken = tokens[2];
  // var secretBuffer = new Buffer(process.env.AUTH0_SECRET);

  try {
    var cert = await fetchAuth0Certfication(idToken);
    await jwtVerify(idToken, cert);
  } catch (e) {
    console.log(`Failed jwt verification: ${e} auth: ${authToken}`);
    callback("Authorization Failed");
  }

  var config = {
    method: "POST",
    url: `https://${process.env.DOMAIN}/userinfo`,
    data: { access_token: accessToken }
  };

  try {
    const response = await axios(config);
    // console.log("data", response.data);
    // console.log("status", response.status);
    // console.log("status_text", response.statusText);
    // console.log("headers", response.headers);
    // console.log("config", response.config);
    if (response.status === 200) {
      callback(null, response.data);
    }
  } catch (e) {
    console.log(e.response.data);
    console.log(e.response.status);
    console.log(e.response.statusText);
    console.log(e.response.headers);
    console.log(e.response.config);
    console.log(e.request);
    console.log(e.message);
    callback(e);
  }
};
