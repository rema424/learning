const chai = require("chai");
const sinon = require("sinon");
const rewire = require("rewire");
const expect = chai.expect;
const assert = chai.assert;
// 変更1: chaiにsinonを読み込む
chai.use(require("sinon-chai"));

const sampleData = {
  Contents: [
    {
      Key: "file1.mp4",
      bucket: "my-bucket"
    },
    {
      Key: "file2.mp4",
      bucket: "my-bucket"
    }
  ]
};

describe("LambdaFunction", function() {
  let listObjectsStub, callbackSpy, module;

  describe("#execute", function() {
    // 変更2: mocha で async/await を使う
    // 変更3: 引数で done は渡さなくてよい
    before(async function() {
      // 変更4: タイムアウトしないように設定する
      // 注記1: アロー関数を使うと this が使えないので function で宣言しておく
      this.timeout(0);
      // 変更5: スタブがpromiseで戻り値を返却するようにする
      listObjectsStub = sinon.stub().returns({ promise: () => sampleData });
      callbackSpy = sinon.spy();

      const callback = function(error, result) {
        callbackSpy.apply(null, arguments);
        // 変更6: ここで done() を呼んで終了を通知する必要はない
      };

      module = getModule(listObjectsStub);
      // 変更7: await で実行する await が終わるまで待ってくれるので終了の通知はいらない
      await module.handler(null, null, callback);
    });

    it("should run our function once", function() {
      // 注記2: chai に sinon を読み込んでおかないと、ここでエラーが発生する
      expect(callbackSpy).has.been.calledOnce;
    });

    it("should have correct results", function() {
      const result = {
        // 注記3: これらは実際のソース上では環境変数で読み込んでいるため、
        // 　　　　ローカルでも環境変数を設定しないとテストに失敗する
        baseUrl: "https://s3.amazonaws.com",
        bucket: "serverless-video-transcoded-rema",
        urls: [
          {
            Key: sampleData.Contents[0].Key,
            bucket: "my-bucket"
          },
          {
            Key: sampleData.Contents[1].Key,
            bucket: "my-bucket"
          }
        ]
      };

      assert.deepEqual(callbackSpy.args, [[null, result]]);
    });
  });
});

function getModule(listObjectsStub) {
  let rewired = rewire("../index.js");

  rewired.__set__({
    s3: { listObjects: listObjectsStub }
  });

  return rewired;
}
