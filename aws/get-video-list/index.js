"use strict";

const AWS = require("aws-sdk");

const s3 = new AWS.S3();

const createErrorResponse = (code, message, encoding) => {
  const response = {
    statusCode: code,
    headers: { "Access-Control-Allow-Origin": "*" },
    body: JSON.stringify({ code, message, encoding })
  };
  return response;
};

const createSuccessResponse = result => {
  const response = {
    statusCode: 200,
    headers: { "Access-Control-Allow-Origin": "*" },
    body: JSON.stringify(result)
  };
  return response;
};

const createBucketParams = () => {
  const params = {
    Bucket: process.env.BUCKET,
    EncodingType: "url"
  };

  return params;
};

const getVideosFromBucket = async params => {
  try {
    const data = await s3.listObjects(params).promise();
    return data;
  } catch (err) {
    throw new Error(err);
  }
};

const createList = (encoding, data) => {
  let files = [];
  for (let index in data.Contents) {
    const file = data.Contents[index];

    if (encoding) {
      const type = file.Key.substr(file.Key.lastIndexOf("-") + 1);
      if (type !== encoding + ".mp4") {
        continue;
      }
    } else {
      if (file.Key.slice(-4) !== ".mp4") {
        continue;
      }
    }

    const params = {
      Bucket: process.env.BUCKET,
      Key: file.Key
    };

    const url = s3.getSignedUrl("getObject", params);

    files.push({
      filename: url,
      eTag: file.ETag.replace(/"/g, ""),
      size: file.Size
    });
  }

  const result = {
    domain: process.env.BASE_URL,
    bucket: process.env.BUCKET,
    files
  };

  return result;
};

exports.handler = async (event, context, callback) => {
  let encoding = null;

  if (event.queryStringParameters && event.queryStringParameters.encoding) {
    encoding = decodeURIComponent(event.queryStringParameters.encoding);
  }

  try {
    const params = createBucketParams();
    const videos = await getVideosFromBucket(params);
    const videoList = createList(encoding, videos);

    if (videoList.files.length > 0) {
      callback(null, createSuccessResponse(videoList));
    } else {
      callback(null, createErrorResponse(404, "No files were found", encoding));
    }
  } catch (err) {
    console.log("error");
    callback(null, createErrorResponse(500, err, encoding));
  }
};
