let uploadController = {
  data: {
    config: null
  },
  uiElements: {
    uploadButton: null
  },
  init: function(configConstants) {
    this.uiElements.uploadButton = $("#upload");
    this.uiElements.uploadButtonContainer = $("#upload-video-button");
    this.uiElements.uploadProgressBar = $("#upload-progress");

    this.data.config = configConstants;

    this.wireEvents();
  },
  upload: function(file, data, that) {
    this.uiElements.uploadButtonContainer.hide();
    this.uiElements.uploadProgressBar.show();
    this.uiElements.uploadProgressBar.find(".progress-bar").css("width", "0");

    let fd = new FormData();
    fd.append("key", data.key);
    fd.append("acl", "private");
    fd.append("Content-Type", file.type);
    fd.append("AWSAccessKeyId", data.access_key);
    fd.append("policy", data.encoded_policy);
    fd.append("signature", data.signature);
    fd.append("file", file, file.name);

    const config = {
      url: data.upload_url,
      type: "POST",
      data: fd,
      processData: false,
      contentType: false,
      xhr: this.progress,
      beforeSend: req => req.setRequestHeader("Authorization", "")
    };

    $.ajax(config)
      .done(response => {
        that.uiElements.uploadButtonContainer.show();
        that.uiElements.uploadProgressBar.hide();
        alert("Uploaded Finished");
      })
      .fail(response => {
        that.uiElements.uploadButtonContainer.show();
        that.uiElements.uploadProgressBar.hide();
        alert("Failed to upload");
      });
  },
  progress: function() {
    let xhr = $.ajaxSettings.xhr();
    xhr.upload.onprogress = evt => {
      let percentage = (evt.loaded / evt.total) * 100;

      $("#upload-progress")
        .find(".progress-bar")
        .css("width", percentage + "%");
    };
    return xhr;
  },
  wireEvents: function() {
    this.uiElements.uploadButton.on("change", result => {
      let file = $("#upload").get(0).files[0];

      let requestDocumentUrl =
        this.data.config.apiBaseUrl +
        "/s3-policy-document?filename=" +
        encodeURI(file.name);

      $.get(requestDocumentUrl, (data, status) => {
        this.upload(file, data, this);
      });
    });
  }
};
