var userController = {
  /**
   * 変数の宣言
   */
  data: {
    webAuth: null,
    config: null,
    profile: null
  },
  /**
   * エレメントの宣言
   */
  uiElements: {
    loginButton: null,
    logoutButton: null,
    profileButton: null,
    profileNameLabel: null,
    profileImage: null,
    uploadButton: null
  },
  /**
   * 初期化メソッド
   */
  init: function(config) {
    // エレメントの代入
    this.uiElements.loginButton = $("#auth0-login");
    this.uiElements.logoutButton = $("#auth0-logout");
    this.uiElements.profileButton = $("#user-profile");
    this.uiElements.profileNameLabel = $("#profilename");
    this.uiElements.profileImage = $("#profilepicture");
    this.uiElements.uploadButton = $("#upload-video-button");

    // データの代入
    this.data.config = config;
    this.data.webAuth = new auth0.WebAuth({
      domain: config.auth0.domain,
      clientID: config.auth0.clientId,
      redirectUri: config.auth0.redirectUri,
      responseType: "token id_token",
      scope: "openid profile email user_metadata picture"
    });

    // ログインが無効（有効期限切れ）の場合、ローカルストレージを削除する
    if (!this.isAuthentication()) {
      this.removeSession();
      this.profile = null;
    }

    // 認証、URLパラメータ、プロフィール表示、UIのハンドリングを行う
    this.handleAuthentication();

    // UIにイベントをセットする
    this.wireEvents();
  },
  /**
   * URLからパラメータを読み取り認証を行う
   */
  handleAuthentication: function() {
    var that = this;

    this.data.webAuth.parseHash(function(err, authResult) {
      // URLから認証情報が得られたらローカルストレージに保存、得られなければ何もしない
      if (authResult && authResult.accessToken && authResult.idToken) {
        window.location.hash = "";
        that.setSession(authResult);
      } else if (err) {
        console.log(err);
        alert(
          "Error: " + err.error + ". Check the console for further details."
        );
      }

      // ローカルストレージにユーザー情報がある場合の処理
      if (localStorage.getItem("id_token")) {
        that.configureAuthenticatedRequests();
        that.getProfile();
      }
    });
  },
  /**
   * ローカルストレージに認可・認証情報を保存する
   */
  setSession: function(authResult) {
    var expiresAt = JSON.stringify(
      authResult.expiresIn * 1000 + new Date().getTime()
    );
    localStorage.setItem("access_token", authResult.accessToken);
    localStorage.setItem("id_token", authResult.idToken);
    localStorage.setItem("expires_at", expiresAt);
  },
  /**
   * ローカルストレージの認可・認証情報を削除する
   */
  removeSession: function() {
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
  },
  /**
   * 認証情報が有効であるか判定する
   */
  isAuthentication: function() {
    var expiresAt = JSON.parse(localStorage.getItem("expires_at"));
    return new Date().getTime() < expiresAt;
  },
  /**
   * プロフィール情報を取得する
   */
  getProfile: function() {
    var accessToken = localStorage.getItem("access_token");
    if (!accessToken) {
      console.log("Access Token must exist to fetch profile");
    }

    var that = this;
    this.data.webAuth.client.userInfo(accessToken, function(err, profile) {
      if (err) {
        return alert("There was an error getting the profile: " + err.message);
      } else if (profile) {
        that.data.profile = profile;
        that.showUserAuthenticationDetails(profile);
      }
    });
  },
  /**
   * プロフィール情報をエレメントに紐付ける
   */
  showUserAuthenticationDetails: function(profile) {
    var showAuthenticationElements = !!profile;

    if (showAuthenticationElements) {
      this.uiElements.profileNameLabel.text(profile.nickname);
      this.uiElements.profileImage.attr("src", profile.picture);
      this.uiElements.uploadButton.css("display", "inline-block");
    }

    this.uiElements.loginButton.toggle(!showAuthenticationElements);
    this.uiElements.logoutButton.toggle(showAuthenticationElements);
    this.uiElements.profileButton.toggle(showAuthenticationElements);
  },
  /**
   * Ajax通信のヘッダー情報をセットする
   */
  configureAuthenticatedRequests: function() {
    var idToken = localStorage.getItem("id_token");
    var accessToken = localStorage.getItem("access_token");
    $.ajaxSetup({
      beforeSend: function(xhr) {
        xhr.setRequestHeader(
          "Authorization",
          `Bearer ${idToken} ${accessToken}`
        );
      }
    });
  },
  /**
   * イベントリスナーをセットする
   */
  wireEvents: function() {
    // 階層が深くなった場合に備えてuserControllerインスタンスを変数に退避
    var that = this;

    // ログインボタンのクリックイベント
    this.uiElements.loginButton.click(function(e) {
      e.preventDefault();
      that.data.webAuth.authorize();
    });

    // ログアウトボタンのクリックイベント
    this.uiElements.logoutButton.click(function(e) {
      that.removeSession();

      that.uiElements.logoutButton.hide();
      that.uiElements.profileButton.hide();
      that.uiElements.uploadButton.hide();
      that.uiElements.loginButton.show();
    });

    // プロフィールボタンのクリックイベント
    this.uiElements.profileButton.click(function(e) {
      var url = that.data.config.apiBaseUrl + "/user-profile";

      $.get(url, function(data, status) {
        alert(JSON.stringify(data));
        // $("#user-profile-raw-json").text(JSON.stringify(data, null, 2));
        // $("#user-profile-modal").modal();
      });
    });
  }
};
