const videoController = {
  data: {
    config: null,
    videoInitialized: false
  },
  uiElements: {
    videoCardTemplate: null,
    videoList: null,
    loadingIndicator: null
  },
  init: function(config) {
    this.uiElements.videoCardTemplate = $("#video-template");
    this.uiElements.videoList = $("#video-list");
    this.uiElements.loadingIndicator = $("#loading-indicator");

    this.data.config = config;

    this.connectToFirebase();
  },
  addVideoToScreen: function(videoObjs) {
    const that = this;

    $.each(videoObjs.urls, function(index, value) {
      const newVideoElement = that.uiElements.videoCardTemplate
        .clone()
        .attr("id", value.firebaseId);

      newVideoElement.click(function() {
        const video = newVideoElement.find("video").get(0);

        if (newVideoElement.is(".video-playing")) {
          video.pause();
          $(video).removeAttr("controls");
        } else {
          $(video).attr("controls", "");
          video.play();
        }

        newVideoElement.toggleClass("video-playing");
      });
      if (value.url) {
        newVideoElement.find("video").attr("src", value.url);
        newVideoElement.find(".transcoding-indicator").hide();
      } else {
        newVideoElement.find("video").hide();
      }
      that.uiElements.videoList.prepend(newVideoElement);
    });
  },
  updateVideoOnScreen: function(firebaseId, videoObj) {
    if (!document.getElementById(firebaseId)) {
      this.addVideoToScreen({ urls: [{ firebaseId, key: videoObj.key }] });
    }

    const videoElement = this.getElementForVideo(firebaseId);
    if (!videoObj) {
      return;
    }
    if (videoObj.transcoding) {
      videoElement.find("video").hide();
      videoElement.find(".transcoding-indicator").show();
    } else {
      videoElement.find("video").show();
      videoElement.find(".transcoding-indicator").hide();

      this.getSignedUrls([{ firebaseId, key: videoObj.key }], function({
        urls
      }) {
        const upload = urls[0];
        videoElement.find("video").attr("src", upload.url);
      });
    }
  },
  getElementForVideo: function(videoId) {
    return $("#" + videoId);
  },
  connectToFirebase: function() {
    const that = this;

    firebase.initializeApp(this.data.config.firebase);

    const database = firebase.database();
    const isConnectedRef = database.ref(".info/connected");
    const nodeRef = database.ref();
    const childRef = database.ref("videos");

    isConnectedRef.on("value", function(snap) {
      if (snap.val() === true) {
        that.uiElements.loadingIndicator.hide();
      }
    });

    // fired when a new movie is added to firebase
    childRef.on("child_added", function(data) {
      if (that.data.videoInitialized) {
        const videoObj = {
          urls: [{ firebaseId: data.key, url: "" }]
        };
        that.addVideoToScreen(videoObj);
      }
    });

    // 画面読み込み時に発火する
    nodeRef.on("child_added", function(data) {
      console.log("node added");
      if (!that.data.videoInitialized) {
        that.data.videoInitialized = true;
        that.getSignedUrls(data.val(), that.addVideoToScreen);
      }
    });

    // fired when a movie is updated
    childRef.on("child_changed", function(data) {
      console.log("child changed");
      console.log(data.key);
      console.log(data.val());
      that.updateVideoOnScreen(data.key, data.val());
    });
  },
  getSignedUrls: function(videoObjs, callback) {
    const that = this;

    if (videoObjs) {
      const objectMap = $.map(videoObjs, function(value, key) {
        return { firebaseId: key, key: value.key };
      });

      const getSignedUrl = this.data.config.apiBaseUrl + "/signed-url";

      // https://medium.com/@lakshmanLD/lambda-proxy-vs-lambda-integration-in-aws-api-gateway-3a9397af0e6d
      // http://www.koikikukan.com/archives/2012/10/02-005555.php
      const promise = $.ajax({
        type: "POST",
        url: getSignedUrl,
        crossDomain: true,
        data: JSON.stringify(objectMap),
        contentType: "application/json",
        dataType: "json"
      });

      promise
        .done((data, status) => {
          console.log("success");
          if (status === "success") {
            callback.call(that, data);
          }
        })
        .fail((data, status) => {
          console.log("failed");
        })
        .always((data, status) => {
          console.log("always");
        });
    }
  }
};
