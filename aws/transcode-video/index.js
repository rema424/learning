"use strict";

const AWS = require("aws-sdk");
const admin = require("firebase-admin");
const serviceAccount = require(process.env.SERVICE_ACCOUNT);

const elasticTranscoder = new AWS.ElasticTranscoder({
  region: process.env.ELASTIC_TRANSCODER_REGION
});

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.DATABASE_URL
});

const pushVideoEntryToFirebase = (key, callback) => {
  const database = admin.database().ref();

  database
    .child("videos")
    .child(key)
    .set({ transcoding: true })
    .then(() => {
      callback(null, "Video record saved to firebase");
    })
    .catch(err => {
      callback(err);
    });
};

exports.handler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  const key = event.Records[0].s3.object.key;

  // '+'をスペースで置き換える
  const sourceKey = decodeURIComponent(key.replace(/\+/g, " "));

  // 拡張子を抜いたファイル名を取得する
  const outputKey = sourceKey.split(".")[0];

  const uniqueVideoKey = outputKey.split("/")[0];
  console.log("sourceKey: ", sourceKey);
  console.log("outputKey: ", outputKey);
  console.log("uniaueVideoKey: ", uniqueVideoKey);

  // トランスコーダーのジョブを実行するのに必要なパラメータを準備する
  const params = {
    PipelineId: process.env.ELASTIC_TRANSCODER_PIPELINE_ID,
    // OutputKeyPrefix: outputKey + "/",
    Input: {
      Key: sourceKey
    },
    Outputs: [
      // {
      //   Key: outputKey + "-1080p" + ".mp4",
      //   PresetId: "1351620000001-000001" //Generic 1080p
      // },
      {
        Key: outputKey + "-720p" + ".mp4",
        PresetId: "1351620000001-000010" //Generic 720p
      }
      // {
      //   Key: outputKey + "-web-720p" + ".mp4",
      //   PresetId: "1351620000001-100070" //Web Friendly 720p
      // }
    ]
  };

  // トランスコーダーのジョブを実行する
  elasticTranscoder.createJob(params, (error, data) => {
    if (error) {
      console.log("Error creating elastic transcoder job.");
      // このコールバックが実行されない場合はLambdaはデフォルトでコールバックを実行する
      callback(error);
      return;
    }
    console.log("Elastic transcoder job created successfully");
    pushVideoEntryToFirebase(uniqueVideoKey, callback);
  });
};
