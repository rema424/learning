/**
 * Created by Peter Sbarski
 * Serverless Architectures on AWS
 * http://book.acloud.guru/
 * Last Updated: Feb 11, 2017
 */

"use strict";
var AWS = require("aws-sdk");
var exec = require("child_process").exec;
var fs = require("fs");

process.env["PATH"] =
  process.env["PATH"] + ":" + process.env["LAMBDA_TASK_ROOT"];

var s3 = new AWS.S3();

/**
 * メタデータをS3に保存する。
 * @param {*} body
 * @param {*} bucket
 * @param {*} key
 * @param {*} callback
 */
function saveMetadataToS3(body, bucket, key, callback) {
  console.log("Saving metadata to s3");

  s3.putObject(
    {
      Bucket: bucket,
      Key: key,
      Body: body
    },
    function(error, data) {
      if (error) {
        callback(error);
      }
    }
  );
}

/**
 * ffprobeを使って動画ファイルのメタデータを取得する。
 * 取得したデータをS3にアップロードする関数を呼び出す。
 * @param {*} sourceBucket
 * @param {*} sourceKey
 * @param {*} localFilename
 * @param {*} callback
 */
function extractMetadata(sourceBucket, sourceKey, localFilename, callback) {
  console.log("Extracting metadata");

  var cmd = `bin/ffprobe -v quiet -print_format json -show_format "/tmp/${localFilename}"`;

  exec(cmd, function(error, stdout, stderr) {
    if (error === null) {
      var metadataKey = sourceKey.split(".")[0] + ".json";
      saveMetadataToS3(stdout, sourceBucket, metadataKey, callback);
    } else {
      console.log(stderr);
      callback(error);
    }
  });
}

/**
 * S3の動画ファイルをlambdaのOS上に読み込む
 * @param {*} sourceBucket
 * @param {*} sourceKey
 * @param {*} callback
 */
function saveFileToFilesystem(sourceBucket, sourceKey, callback) {
  console.log("Saving to filesystem");

  var localFilename = sourceKey.split("/").pop();
  var file = fs.createWriteStream(`/tmp/${localFilename}`);

  // S3のファイルをlambdaのOS上に読み込む
  var stream = s3
    .getObject({
      Bucket: sourceBucket,
      Key: sourceKey
    })
    .createReadStream()
    .pipe(file);

  // 読み込み時エラー処理
  stream.on("error", function(error) {
    callback(error);
  });

  // 読み込みが完了したら、読み込んだファイルのメタデータを作成する。
  // 作成したデータはS3にアップする。
  stream.on("close", function() {
    extractMetadata(sourceBucket, sourceKey, localFilename, callback);
  });
}

exports.handler = function(event, context, callback) {
  console.log("Welcome");
  console.log(event);

  // パラメータとして渡ってきたeventの中身を抽出する。
  // 値が文字列型なのでJsonに変換する。
  var message = JSON.parse(event.Records[0].Sns.Message);

  var sourceBucket = message.Records[0].s3.bucket.name;
  var sourceKey = decodeURIComponent(
    message.Records[0].s3.object.key.replace(/\+/g, " ")
  );

  saveFileToFilesystem(sourceBucket, sourceKey, callback);
};
